#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    ElmerKeywordExtractor
    Copyright (C) 2021  Thomas Wiesner

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import sys
from datetime import datetime

VERSION = '2021.12.29'

usage = """ElmerKeywordExtractor Copyright (C) 2021  Thomas Wiesner
This program comes with ABSOLUTELY NO WARRANTY; for details see the source
code.

ElmerKeywordExtractor version {}.

Extract Elmer (http://www.elmerfem.org/blog/, https://www.csc.fi/web/elmer)
SIF (solver input file) keywords from Fortran source code and generate a HTML
summary. Read the accompanying README.md file for limitations.

Command line parameters:
    -h: Print this info an exit.
    -i input_file: Add 'input_file' to the list of files to parse. If the
        specified file is a regular file, it must contain valid Fortran source
        code. If 'input_file' is a directory, the directory is recursively
        scanned for files with the extension .F90 or .f90 which are assumed to
        be the source code files to scan. This parameter can be given multiple
        times to parse multiple files or directories.

    -s skip_file: Optional. Same as -i, but all files and directories
        specified or found are excluded from the list of files to parse.
        
    -o output_file: Name of the output file to write.
    
    -openFoldings: Set the default state of the HTML output folding sections
        to open instead of closed. Useful, because some browsers don't search
        closed folding sections with full text search.

    -k SOLVER.KEYWORDS: Optional. Name/path of the SOLVER.KEYWORDS file. The
        keyword extractor parses the keyword file and matches the keywords
        found in the Fortran source code with the keyword list.
""".format(VERSION)


class KeywordReader:
    unknownType = 'unknown'
    
    def __init__(self, name, dtype, argNr):
        self.name = name    # Fortran function/subroutine name
        self.dtype = dtype  # Keyword type as string
        self.argNr = argNr  # Argument number containing the keyword name
        
    def __repr__(self):
        return 'name: ' + self.name + ', dtype: ' + self.dtype + ', argNr: ' + str(self.argNr)

class Keyword:
    def __init__(self, name, KWreader, sourceFile, sourceLineNumber, cleanedSourceLine):
        self.name = name
        self.KWreader = KWreader
        self.sourceFile = sourceFile
        self.sourceLineNumber = sourceLineNumber
        self.cleanedSourceLine = cleanedSourceLine
        
    def __repr__(self):
        return 'name: ' + self.name + \
            ', KeywordReader: (' + self.KWreader.__repr__() + ')' + \
            ', source: ' + self.sourceFile

class FortranLine:
    def __init__(self, cleanedLine, sourceLineNr):
        self.cleanedLine = cleanedLine
        self.sourceLineNr = sourceLineNr
        
    def __repr__(self):
        return '{:5d}: {}'.format(self.sourceLineNr, self.cleanedLine)

class SolverKeywordsLine:
    # Holds data from parsing SOLVER.KEYWORDS
    def __init__(self, KWname, KWsection, KWtype, sourceLineNr):
        self.KWname = KWname
        self.KWsection = KWsection
        self.KWtype = KWtype
        self.sourceLineNr = sourceLineNr
    
    def __repr__(self):
        return '{:5d}: {}:{}:{}'.format(self.sourceLineNr, self.KWsection, self.KWtype, self.KWname)

def readFortran(inFile):
    """
        Reads the Fortran file specified by the filename string inFile.
        Removes full-line comments, leading and trailing whitespace and
        combines continued Fortran lines (with &) into single lines.
        
        Returns: A list of string, one entry per output line.
    """
    comment = '!'   # String initiating a comment
    continuation = '&'  # String for triggering line continuation
    
    fp = open(inFile, 'r')
    
    lines = []
    continueLine = False
    continuationMarkFound = False
    
    lineNr = 0
    
    for line in fp:
        lineNr += 1
        
        line = line.strip()
        line = stripFortranComment(line)
        
        if line.startswith(comment):
            # Full line comment. Skip.
            continue
            
        if line.endswith(continuation):
            # Remove continuation mark
            line = line[0:-len(continuation)]
            
            continuationMarkFound = True
        else:
            continuationMarkFound = False
        
        if len(line) < 1:
            # Skip empty lines.
            continue
            
        if line.startswith(continuation):
            # A continuation mark can also be located at the beginning of a line.
            # This this case, there must have been one at the previous line.
            if not continueLine:
                print('Error in line {}: & Line continuation at beginng of line without & at end of previous line.'.format(lineNr))
                
            # Remove continuation mark.
            line = line[len(continuation):]
        
        if continueLine:
            # This line is a continuation of the previous one. Append.
            lines[-1].cleanedLine = lines[-1].cleanedLine + line
        else:
            lines.append(FortranLine(line, lineNr))
    
        continueLine = continuationMarkFound    
        
    fp.close()
    
    return lines

def isFortranString(sstr):
    return sstr[0] == '"' or sstr[0] == "'"

def stripFortranComment(line):
    comment = '!'
    str1 = "'"
    str2 = '"'
    
    cmtPos = line.find(comment)
    
    # The following two cases are just shortcuts and avoid parsing the while line in detail
    # in most cases.
    
    
    if cmtPos < 0:
        # No comment char in the whole line => No comment. Nothing to do.
        return line
    
    if line.find(str1) < 0 and line.find(str2) < 0:
        # There is a comment char in the line and no string delimiters. Consequently,
        # the comment char must be at the end of the line. Just truncate.
        line = line[0:cmtPos]
            
        # Removing the comment usually causes some trailing whitespace. Remove.
        line = line.strip()
        
        return line

    
    # Remaining cases. Either comment chars in strings or string chars in comment.
    # Start at the beginning of the line and parse until a comment char not in a string is found.
    
    pos = 0
    
    # Scan until we find either a string or a commend char.
    while True:
        while pos < len(line) and line[pos] != comment and not isFortranString(line[pos]):
            pos += 1
        
        if pos >= len(line):
            return line.strip()
        
        if line[pos] == comment:
            # Comment char outside of a string. Just truncate.
            line = line[0:pos]
            
            return line.strip()
        
        # If we get here, we have a string. Parse it.
        fstr, pos = readFortranString(line, pos)
        
        if pos < 0:
            print('stripFortranComment: Error parsing Fortran string with readFortranString(). ret = {}'.format(pos))
            return line.strip()
    
    return line.strip()

def readFortranString(line, pos):
    """
        Reads a Fortran string from line begining at pos.
        
        Returns a tuple containing the string contents and the position of the first character
        after the string.
        Returns negative position values on error.
    """

    fstr = ''

    if not isFortranString(line[pos]):
        # pos does not point to the beginning of a string.
        return ('', -1)
    
    # Store string delimiter and skip it.
    schar = line[pos]
    pos += 1
    
    while pos < len(line):
        if line[pos] == schar:
            #print('String delimiter at {}'.format(pos))
            if pos < len(line) - 1 and line[pos+1] == schar:
                fstr = fstr + schar
                
                #print('Escaping delimiter at {}'.format(pos))
                
                # Found an escaped string delimiter. Skip it.
                pos += 1
            else:
                # End of string found.
                #print('EOS at {}'.format(pos))
                return (fstr, pos+1)
        else:
            fstr = fstr + line[pos]
        
        pos += 1
    
    #print('pos {}'.format(pos))
    
    # Arrived at the end of the line without finding the end of a string.
    # This is an error.
    print('readFortranString: Unterminated Fortran string in "{}"'.format(line))

    return (fstr, -2)    

def getKeywordReader(sourceFile, line, readerList):
    # Combine whitespace and do some initial checks.
    # Soubroutine and function defintions don't lead to useful
    # output.
    c = line.cleanedLine.lower().split()
    
    if len(c) > 0 and (c[0] == 'subroutine' or c[0] == 'function'):
        return None

    if len(c) > 1 and c[0] == 'recursive' and (c[1] == 'function' or c[1] == 'subroutine'):
        return None
    
    if len(c) > 1 and c[0] == 'end' and (c[1] == 'function' or c[1] == 'subroutine'):
        return None
    
    # Iterate over all given readers and see if we can a matching one.
    cnt = 0
    readerFound = None
    for i, reader in enumerate(readerList):
        sidx = line.cleanedLine.find(reader.name)
        
        if sidx >= 0:
            # Be greedy and use the longest hit.
            if readerFound is not None:
                if reader.name.find(readerFound.name) >= 0:
                    # Old hit is a substring of the new hit. The new is a longer match.
                    #print('getKeywordReader: Info (source line {}): Changing match {} to longer match {}'.format(line.sourceLineNr, readerFound.name, reader.name))
                    readerFound = reader
                elif readerFound.name.find(reader.name) >= 0:
                    # Old hit is already the longer match. Nothing to do.
                    continue
                else:
                    cnt += 1
                    #print('### ' + reader.name + ', ' + readerFound.name)
                    #print('###   ' + str(reader.name.find(readerFound.name)) + ", " + str(readerFound.name.find(reader.name)))
                
                    kidx = sidx
                    readerFound = reader
            else:
                cnt += 1
            
                kidx = sidx
                readerFound = reader

    if cnt == 0:
        return None
    
    if cnt > 1:
        # Note: This detection is not reliable. If one keyword is a substring of another on the same line
        # like GetReal and GetRealVector, the shorter keyword may be unnoticed.
        print('getKeywordReader: Found more than one possible keyword reading method. Cannot deal with this.')
        print('getKeywordReader:   Input line: ' + line.cleanedLine)
        print('getKeyWordReader:   Source line ' + str(line.sourceLineNr))
        return None

    args = getFortranFunctionArguments(line.cleanedLine[kidx:], line.sourceLineNr)

    #print('getKeywordReader: Found {} in line "{}". Extracted parameters:'.format(readerFound.name, line))
    #print('   ' + args[readerFound.argNr])

    # Since we didn't really parse the Fortran syntax to find function calls but only
    # searched for the function strings, we may    
    if len(args) < 1:
        print('getKeywordReader: Could not find arguments of possible function {} in line {}. Skipping.'.format(readerFound.name, line.sourceLineNr))
        return None
    
    if readerFound.argNr >= len(args):
        print('getKeywordReader: To few arguments in possible call of {} in line {}. Skipping.'.format(readerFound.name, line.sourceLineNr))
        return None
    
    return Keyword(args[readerFound.argNr], readerFound, sourceFile, line.sourceLineNr, line.cleanedLine)


KWreaders = [# Lists.F90
             KeywordReader('ListGetInteger', 'Integer', 1),
             KeywordReader('ListGetIntegerArray', 'Integer', 1),
             KeywordReader('ListGetLogical', 'Logical', 1),
             KeywordReader('ListGetLogicalGen', 'Logical', 1),
             KeywordReader('ListGetString', 'String', 1),
             KeywordReader('ListGetConstReal', 'Real', 1),
             KeywordReader('ListGetCReal', 'Real', 1),
             KeywordReader('ListGetRealAtNode', 'Real', 1),
             KeywordReader('ListGetReal', 'Real', 1),
             KeywordReader('ListGetRealArray', 'Real', 1),
             KeywordReader('ListGetFun', 'Real', 1),
             KeywordReader('ListGetConstRealArray', 'Real', 1),
             KeywordReader('ListGetConstRealArray1', 'Real', 1),
             KeywordReader('ListGetRealVector', 'Real', 1),
             KeywordReader('ListInitElementKeyword', KeywordReader.unknownType, 2),    # Type unknown. Depends on the successive ListGetElement* call.
             # DefUtils.F90             
             KeywordReader('GetReal', 'Real', 1),
             KeywordReader('GetRealVector', 'Real', 2),
             KeywordReader('GetString', 'String', 1),
             KeywordReader('GetInteger', 'Integer', 1),
             KeywordReader('GetLogical', 'Logical', 1),
             KeywordReader('GetConstReal', 'Real', 1),
             KeywordReader('GetCReal', 'Real', 1),
             KeywordReader('GetRealValues', 'Real', 1),
             KeywordReader('GetParentMatProp', KeywordReader.unknownType, 0),
             KeywordReader('GetConstRealArray', 'Real', 2),
             KeywordReader('GetRealArray', 'Real', 2),
             KeywordReader('GetComplexVector', 'Complex', 2)
             ]

def buildFortranFileList(topDir):
    fn = []
    
    for root, dirs, files in os.walk(topDir, topdown=False):
        for name in files:
            #print(os.path.join(root, name))
            
            file = os.path.join(root, name)
            
            f_name, f_extension = os.path.splitext(file)
            
            if f_extension.lower() == '.f90':
                fn.append(file)
            
    return fn

def parseUntilClosing(line, idx, openingBracket, closingBracket):
    """
        Returns the string until the closing bracket of type 'bracket' of the
        current level. Also returns the index of the the closing bracket.
        idx is the starting index.
    """
    
    slen = len(line)
    
    arg = ''
    
    while idx < slen and line[idx] != closingBracket:
        if line[idx] == openingBracket:
            # Next bracket level. Recursive call.
            sstr, idx = parseUntilClosing(line, idx+1, openingBracket, closingBracket)
            
            if idx < slen:
                arg += openingBracket + sstr + closingBracket
            else:
                # Avoids introducing spurious closing bracket that if brackets are unmatched.
                arg += openingBracket + sstr
                
            idx += 1
        else:
            arg += line[idx]
            idx += 1
    
    if idx >= slen:
        print('parseUntilClosing: Warning: Unmatched bracket.')
    
    return (arg, idx)
    

def getFortranFunctionArguments(call, line):
    """
        Expects the string call to start at the beginning of a Fortran function call.
        Returns a list of all function arguments.
        
        Works only for rather simple arguments, since it is not a full Fortran parser.
    """
    
    str1 = '"'
    str2 = "'"

    args = []
    
    idx = call.find('(')
    
    if idx < 0:
        print('getFortranFunctionArgument: Error at source line {}: Could not find opening bracket of function call.'.format(line))
        print('getFortranFunctionArgument:   call: ' + call)
        return args
    
    slen = len(call)

    idx += 1
    # idx points at the first character after the function argument bracket.
    
    while idx < slen and call[idx] != ')':
        arg = ''
        
        # Skip whitespace
        while idx < slen and call[idx].isspace():
            idx += 1
        
        if idx >= slen or call[idx] == ')':
            break
        
        # Argument reading begins here
        while idx < slen and call[idx] != ',' and call[idx] != ')':
            if call[idx] == str1 or call[idx] == str2:
                # Found a string.
                sstr, idx = readFortranString(call, idx)
                arg = arg + "'" + sstr + "'"
                
            elif call[idx] == '(':
                sstr, idx = parseUntilClosing(call, idx+1, '(', ')')
                arg += '(' + sstr + ')'
                idx += 1
                
            else:
                arg += call[idx]
                idx += 1

        args.append(arg.strip())

        idx += 1

    return args

def parseKeywordsInFiles(fileList, skipList):
    kwds = []
    
    for inFile in fileList:
        if inFile in skipList:
            print('Skipping ' + inFile)
            continue
        
        print('Parsing ' + inFile)
        
        lines = readFortran(inFile)
    
        for line in lines:
            kw = getKeywordReader(inFile, line, KWreaders)
            if kw is not None:
                kwds.append(kw)
    
    return kwds

def isKeywordNameComputed(kw):
    """
        Returns True, if the string kw is not just a Fortran string.
    """

    fstr, pos = readFortranString(kw, 0)
    
    if fstr != kw[1:-1]:
        return True
    
    return False

def makeKeywordReadersTable():
    s = '<h1>Known Fortran Functions</h1>'
    s += '<table class="funtable">\n'

    s += '<tr><th>Fortran function/subroutine</th><th>Return type</th><th>Name parameter number</th></tr>\n'
    
    for k in KWreaders:
        s += '<tr><td>{}</td><td>{}</td><td>{}</td></tr>\n'.format(k.name, k.dtype, k.argNr)
    
    s += '</table>\n'
    
    return s


def dumpKeywordsToHTMLfile(kwds, solverKeywords, outFile, openFoldings):
    header = """<!doctype html>
<meta charset=utf-8>
<title>Elmer Keyword List</title>

<head>
<style>

/*
Summary/details based on https://interactive-examples.mdn.mozilla.net/pages/tabbed/details.html
*/

details {
	border: 1px solid #aaa;
	border-radius: 4px;
	padding: .5em .5em 0;
	margin: 0px 0px 10px 0px;
}

summary {
	font-weight: bold;
	margin: -.5em -.5em 0;
	padding: .5em;
	font-family: sans-serif;
	font-size: 11pt;
	background: #eeeeee;
}

details[open] {
	padding: .5em;
}

details[open] summary {
	border-bottom: 1px solid #aaa;
	margin-bottom: .5em;
}

p {
	font-family: sans-serif;
	font-size: 9pt;
	margin: 0 8em;
	text-indent: -8em;
}

p.ver {
	margin: 0;
	text-indent: 0;
}


h1 {
	font-family: sans-serif;
	font-size: 16pt;
	padding: 14pt 0pt 0pt 2pt;
	border-bottom: 4px solid #888;

}

.cmd {
	font-family: monospace;
}


.kwLine {
	font-family: monospace;
	font-size: 9pt;
}

.kwName {
	font-weight: bold;
	font-family: sans-serif;
	font-size: 9pt;
}

.kwNameComputed {
	font-weight: bold;
	font-family: sans-serif;
	font-size: 9pt;
	color: #900090;
}

.kwFun {
	font-style: italic;
	font-family: sans-serif;
	font-size: 9pt;
}

.kwType {
	color: #007700;
	font-family: sans-serif;
	font-size: 9pt;
}

.kwSource {
	color: blue;
	font-family: sans-serif;
	font-size: 9pt;
}

.nMatch {
	background: #ffcf8a;
}

.ntMatch {
	background: #adffae;
}

table {
	font-family: sans-serif;
	font-size: 9pt;
	border-collapse: collapse;
}

table.kwtable {
	margin: 2pt 0pt 6pt 8em;
	width: 30em;
}

table.funtable {
	margin: 2pt 0pt 6pt 2em;
}

td {
	border: 1px solid #888;
	padding: .1em .5em .1em .5em;
}

th {
	border: 1px solid #888;
	padding: .1em .5em .1em .5em;
	background: #ddd;
}

</style>
</head>

<body>

"""
   
    legend="""<details open><summary>Source file name</summary>
  <p><span class="kwLine">Source line: </span><span class="kwName">'keyword name string'</span> or <span class="kwNameComputed">computed keyword name</span>, <span class="kwFun">Reading Fortran function/subroutine</span>, <span class="kwType">Return type of reading Fortran function</span>, <span class="kwSource">Parsed Fortran line</span><br>
	<br>Matches found in solver keywords file (if such a file is specified):<br>
	<table class="kwtable">
		<tr><td class="nMatch" width="20%" style="text-align:right;font-family:monospace">line</td><td class="nMatch" width="40%">Section</td><td class="nMatch" width="40%">Type does not match</td></tr>
		<tr><td class="ntMatch" width="20%" style="text-align:right;font-family:monospace">line</td><td class="ntMatch" width="40%">Section</td><td class="ntMatch" width="40%">Type matches</td></tr>
	</table>
	</p>
</details>
"""
    
    footer = """</body>"""
    
    lastFile = ''
    fp = open(outFile, 'w')
    
    fp.write(header)
    
    datestr = datetime.now().strftime("%Y.%m.%d, %H:%M:%S")
    
    cmdstr = sys.argv[0]
    for arg in sys.argv[1:]:
        cmdstr += ' ' + arg
    
    fp.write('<p class="ver">Generated by {} version {} on {}<br>'.format(sys.argv[0], VERSION, datestr))
    fp.write('Command line was: <span class="cmd">{}</span></p>\n'.format(cmdstr))
    
    fp.write(makeKeywordReadersTable())
    fp.write('<h1>Legend</h1>')
    fp.write(legend)
    fp.write('<h1>Parsing Results</h1>')
    
    for kw in kwds:
        if kw.sourceFile != lastFile:
            if lastFile != '':
                fp.write('</details>\n')
                
            if openFoldings:
                fp.write('<details open><summary>{}</summary>\n'.format(kw.sourceFile))
            else:
                fp.write('<details><summary>{}</summary>\n'.format(kw.sourceFile))

            lastFile = kw.sourceFile

        linestr = '  {:5d}: '.format(kw.sourceLineNumber)
        linestr = linestr.replace(' ', '&nbsp;')
        
        if isKeywordNameComputed(kw.name):
            sclass = 'kwNameComputed'
        else:
            sclass = 'kwName'
            
        fp.write('  <p><span class="kwLine">{}</span><span class="{}">{}</span>, <span class="kwFun">{}</span>, <span class="kwType">{}</span>, <span class="kwSource">{}</span><br>\n'.format(linestr, sclass, kw.name, kw.KWreader.name, kw.KWreader.dtype, kw.cleanedSourceLine))

        # Find matching keywords from SOLVER.KEYWORDS. Ignore type at fist.
        matches = getMatchingKeywords(solverKeywords, kw.name[1:-1], None)
        
        if len(matches) > 0:
            fp.write('    <table class="kwtable">\n')
            
            for row in matches:
                if row.KWtype.lower() == kw.KWreader.dtype.lower():
                    tclass = 'ntMatch'
                else:
                    tclass = 'nMatch'
                
                fp.write('      <tr>')
                fp.write('<td class="{}" width="20%" style="text-align:right;font-family:monospace">{}</td>'.format(tclass, row.sourceLineNr))
                fp.write('<td class="{}" width="40%">{}</td>'.format(tclass, row.KWsection))
                fp.write('<td class="{}" width="40%">{}</td>'.format(tclass, row.KWtype))
                fp.write('</tr>\n')
            
            fp.write('    </table>\n')


        fp.write('  </p>\n')
    
    fp.write('</details>\n')
    fp.write(footer)
    fp.close()


def readSolverKeywordsFile(inFile):
    """
        Read and parse the Elmer SOLVER.KEYWORDS file.
    """
    lines = readFortran(inFile)
    
    kwds = []
    for line in lines:
        if line.cleanedLine.startswith('$'):
            print('readSolverKeyWordsFile: line {} starts with $. Cannot process MATC expressions. Skipping.'.format(line.sourceLineNr))
            continue
        
        # Lines are of the structure
        #    section:type:      'name'
        # Split into section/type and name part.
        tokens = line.cleanedLine.split(':', maxsplit=2)
        
        if len(tokens) != 3:
            print('readSolverKeyWordsFile: line {} does not contains 3 colon seperated fields. Ignoring.'.format(line.sourceLineNr))
            continue
    
        name = tokens[2].strip()
        name = name[1:-1]   # Remove string start and end characters
        
        section = tokens[0].strip()
        kwtype = tokens[1].strip()

        kwds.append(SolverKeywordsLine(name, section, kwtype, line.sourceLineNr))

    return kwds

def getMatchingKeywords(solverKeywords, KWname, KWtype):
    """
        Return all keywords from the SOLVER.KEYWORDS file parsing results that match
        a given name and type. If type is None, the type is ignored.
        
        Comparisons are performed case insensitive.
    """
    res = []
    
    for kw in solverKeywords:
        if KWtype is None:
            if kw.KWname.lower() == KWname.lower():
                res.append(kw)
        else: 
            if kw.KWname.lower() == KWname.lower() and kw.KWtype.lower() == KWtype.lower():
                res.append(kw)
            
    return res

def checkAndAppendInputFileDir(lst, p):
    if not os.path.exists(p):
        print('Could not find input file or directory ' + p)
        sys.exit()
        
    if os.path.isdir(p):
        lst = lst + buildFortranFileList(p)
    else:
        lst = lst + [p]
        
    return lst


fileList = []
skipList = []
outputFile = None
solverKeywordsFile = None
solverKeywords = []
openFoldings = False

i = 1
N = len(sys.argv)
a = sys.argv
while i < N:
    opt = a[i]
    if opt == '-i' or opt == '-s' or opt == '-o' or opt == '-k':
        i += 1 # Advance argument pointer to parameter
        if i >= N:
            print('Missing argument for option ' + opt)
            sys.exit()
        param = a[i]
            
    if opt == '-i':
        # Parse input file specification
        fileList = checkAndAppendInputFileDir(fileList, param)
    elif opt == '-s':
        # Parse skip file specification
        skipList = checkAndAppendInputFileDir(skipList, param)
    elif opt == '-o':
        # Output file specification
        outputFile = param
    elif opt == '-k':
        # SOLVER.KEYWORDS file specification
        solverKeywordsFile = param
    elif opt == '-h':
        print(usage)
        sys.exit()
    elif opt == '-openFoldings':
        openFoldings = True
    else:
        print('Unknown option "{}." Use option -h for help.\n'.format(opt))
        sys.exit()
    
    i += 1

if not fileList:
    print('Use the -i option to specify input files. Use option -h for help.\n')
    sys.exit()

if outputFile is None:
    print('Use the -o option to specify the output filename. Use option -h for help.\n')
    sys.exit()

kwds = parseKeywordsInFiles(fileList, skipList)

if solverKeywordsFile is not None:
    solverKeywords = readSolverKeywordsFile(solverKeywordsFile)

print('Writing HTML output to {}.'.format(outputFile))
dumpKeywordsToHTMLfile(kwds, solverKeywords, outputFile, openFoldings)


    