# ElmerKeywordExtractor

Extract Elmer (http://www.elmerfem.org/blog/, https://www.csc.fi/web/elmer)
SIF (solver input file) keywords from Fortran source code and generate a HTML summary.

## Usage
To generate a keyword list for alle Elmer FEM source code, run

    ElmerKeywordExtractor.py -i src/ -o keywords.html -k src/SOLVER.KEYWORDS

in the `fem/` source code subdirectory of the Elmer project (https://github.com/ElmerCSC/elmerfem)

## License

ElmerKeywordExtractor is free software. This program comes with ABSOLUTELY NO
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the source code for more details.


## Limitations

- Reading the input files may fail if the encoding is not UTF-8. (Use e.g. `iconv` to convert them.)

- The extractor understands Fortran syntax to a very limited extent; Mostly how comments, line continuations and function/subroutine arguments look like. On loading a Fortran source file, it removes leading and trailing whitespace, removes comments and combines continued lines into single lines.

- Because almost all keywords are read from the SIF using particular functions (defined in the `KWreaders` list of this tool), the source code is then searched for matching names. Once a matching name is found, the function parameter specifying the keyword name is extracted and stored. Strange output may result, if the definition of a keyword reading function is missing but is shares a name part with a defined one.

- The SIF section (for example, `Solver`, `Simulation`, `Boundary Condition`) a keyword belongs to is not extracted, because it is implicitly defined by a variable passed into the Fortran keyword reading functions and could thus be named arbitrarily. For example, it can be seen that the section in `EdgeBasis = .NOT. LFactFound .AND. GetLogical( SolverParams, 'Edge Basis', Found )` is likely the `Solver` section, but `SolverParams` is just a Fortran variable and could have another name.

- Sometimes, even the type cannot be inferred, for example, in combination with the `ListInitElementKeyword` function because it does not spcify the type. The type is determined by the later calls to `ListGetElementReal`, `ListGetElementLogical` and so on, but those functions do not have the name as parameter anymore. It would be necessary to trace the handles between the functions. This is not implemented.

- Not all keywords are read by specialized functions, `echo off` and `numbering on` are examples. Such keywords cannot be found by the extractor.

- The SIF keyword names can be "computed", i.e., assembled at runtime of the solver and depend on other keywords `SpringCoeff(1:n,i,i) = GetReal( BC, ComponentName('Spring',i), GotIt)` is an example. Although Fortran lines defining such computed keywords can usually be extracted, there is no simple way to deduce the final name of the keywords. The extractor marks them in color and uses the Fortran function argument (`ComponentName('Spring',i)` in the example) of the reader-function as a name.

- MATC expressions can be included in the `SOLVER.KEYWORDS` file. Since the extractor does not emulate MATC, it cannot reproduce keyword definitions given by MATC expressions.

- Due to the aforementioned limitations (especially the unknown SIF section), the extractor generates an HTML output entry for each keyword evaluation found in the source code. Multiple evaluations of the same keyword are not combined.


